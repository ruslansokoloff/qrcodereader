package ru.morizo.qrcodereader.core.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.morizo.qrcodereader.core.data_layer.database.AppDatabase
import ru.morizo.qrcodereader.core.di.scope.PerApplication

@Module(includes = [ApplicationModule::class])
class DatabaseModule {

    @Provides
    @PerApplication
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}
