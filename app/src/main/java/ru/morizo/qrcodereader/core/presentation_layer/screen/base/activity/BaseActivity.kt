package ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import ru.morizo.qrcodereader.BR
import ru.morizo.qrcodereader.core.App
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.BaseViewModelFactory
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator

abstract class BaseActivity<VM : ActivityViewModel, B : ViewDataBinding> : AppCompatActivity() {

    abstract val viewModelType: Class<VM>

    @get:LayoutRes
    abstract val layoutIdRes: Int

    @get:IdRes
    abstract val fragmentsContainerId: Int

    val viewModel: VM by lazy {
        getViewModelInstance()
    }

    protected lateinit var binding: B

    private lateinit var navigator: Navigator

    abstract fun provideViewModel(): VM

    abstract fun inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        lifecycle.addObserver(viewModel)

        binding = DataBindingUtil.setContentView(this, layoutIdRes)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this

        navigator = SupportAppNavigator(this, fragmentsContainerId)
    }

    override fun onResume() {
        super.onResume()
        App.cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.cicerone.navigatorHolder.removeNavigator()
    }

    protected open fun getViewModelInstance(): VM {
        return ViewModelProvider(this, getViewModelFactoryInstance()).get(viewModelType)
    }

    fun getApp() = application as App

    private fun getViewModelFactoryInstance(): BaseViewModelFactory<VM> {
        return object : BaseViewModelFactory<VM>() {
            override fun provideViewModel(): VM {
                return this@BaseActivity.provideViewModel().also {
                    it.onViewModelCreated(intent.extras)
                }
            }
        }
    }
}