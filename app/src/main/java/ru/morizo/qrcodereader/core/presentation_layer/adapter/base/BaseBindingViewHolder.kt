package ru.morizo.qrcodereader.core.presentation_layer.adapter.base

import androidx.databinding.ViewDataBinding
import ru.morizo.qrcodereader.BR

abstract class BaseBindingViewHolder<Model, Binding : ViewDataBinding>(val binding: Binding) :
    LiveDataBindingRecyclerViewAdapter.LiveDataBindingViewHolder(binding.root) {

    /**
     * Биндинг элемента к модели для вызова из onBindViewHolder, как правило просто для
     * подстановки переменной в binding
     *
     * @param model Model/ViewModel элемента списка
     */
    open fun bind(model: Model) {
        binding.setVariable(BR.viewModel, model)
    }
}
