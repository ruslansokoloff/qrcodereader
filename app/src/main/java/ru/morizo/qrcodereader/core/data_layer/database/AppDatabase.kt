package ru.morizo.qrcodereader.core.data_layer.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.morizo.qrcodereader.scanner.data_layer.database.dao.CheckDao
import ru.morizo.qrcodereader.scanner.data_layer.database.type_converter.CheckItemBodyConverter
import ru.morizo.qrcodereader.scanner.data_layer.model.entity.CheckEntity
import ru.morizo.qrcodereader.scanner.data_layer.model.entity.EventEntity

@Database(entities = [CheckEntity::class, EventEntity::class], version = AppDatabase.DB_VERSION)
@TypeConverters(CheckItemBodyConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun checkDao(): CheckDao

    companion object {
        const val DB_NAME = "database"
        const val DB_VERSION = 2
    }
}