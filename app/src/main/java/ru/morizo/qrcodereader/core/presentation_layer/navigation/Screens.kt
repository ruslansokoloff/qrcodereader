package ru.morizo.qrcodereader.core.presentation_layer.navigation

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import ru.morizo.qrcodereader.core.presentation_layer.screen.main.MainActivity
import ru.morizo.qrcodereader.core.presentation_layer.screen.splash.SplashActivity
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.checks_history.ChecksHistoryFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner.ScannerFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_result.ScannerResultFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_selector.ScannerSelectorFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.zxing_scanner.ZxingScannerFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    fun splashActivity() = object : SupportAppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            return SplashActivity.newIntent(context)
        }
    }

    fun mainActivity() = object : SupportAppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            return MainActivity.newIntent(context)
        }
    }


    fun scannerSelector() = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ScannerSelectorFragment.newInstance()
        }
    }

    fun scanner() = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ScannerFragment.newInstance()
        }
    }

    fun zxingScanner() = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ZxingScannerFragment.newInstance()
        }
    }

    fun scannerResult(checkPreview: CheckPreview) = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ScannerResultFragment.newInstance(checkPreview)
        }
    }

    fun scannerResult(check: Check) = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ScannerResultFragment.newInstance(check)
        }
    }

    fun checksHistory() = object : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return ChecksHistoryFragment.newInstance()
        }
    }
}