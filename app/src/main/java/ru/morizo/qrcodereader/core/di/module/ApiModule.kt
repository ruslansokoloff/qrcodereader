package ru.morizo.qrcodereader.core.di.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import ru.morizo.qrcodereader.core.data_layer.network.api.MainApi
import ru.morizo.qrcodereader.core.di.scope.PerApplication

@Module(
    includes = [
        ApplicationModule::class,
        HttpModule::class
    ]
)
class ApiModule {

    @Provides
    @PerApplication
    fun provideMainApi(
        builder: Retrofit.Builder,
        httpClient: OkHttpClient
    ): MainApi {
        val httpClientBuilder = httpClient.newBuilder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

        return builder
            .client(httpClientBuilder.build())
            .baseUrl(MainApi.BASE_URL).build()
            .create(MainApi::class.java)
    }
}