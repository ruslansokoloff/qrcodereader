package ru.morizo.qrcodereader.core.presentation_layer.adapter.base

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView

/**
 * Абстрактный RV Adapter для работы с [LiveDataBindingViewHolder]
 */
abstract class LiveDataBindingRecyclerViewAdapter<T : LiveDataBindingRecyclerViewAdapter.LiveDataBindingViewHolder> : RecyclerView.Adapter<T>() {

    /**
     * ViewHolder появляется на экране, триггерим изменение его ЖЦ
     */
    override fun onViewAttachedToWindow(holder: T) {
        super.onViewAttachedToWindow(holder)
        holder.markAttach()
    }

    /**
     * ViewHolder пропадает с экрана, триггерим изменение его ЖЦ
     */
    override fun onViewDetachedFromWindow(holder: T) {
        super.onViewDetachedFromWindow(holder)
        holder.markDetach()
    }

    /**
     * ViewHolder содержащий собственный жизненный цикл для работы LiveDataBinding
     *
     * @property binding Объект биндинга
     * @property lifecycleRegistry Регистр ЖЦ для работы биндинга с LiveData
     */
    open class LiveDataBindingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LifecycleOwner {

        private val lifecycleRegistry = LifecycleRegistry(this)

        init {
            lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
        }

        /** Старт ЖЦ для элемента */
        fun markAttach() {
            lifecycleRegistry.currentState = Lifecycle.State.STARTED
        }

        /** Конец ЖЦ для элемента */
        fun markDetach() {
            lifecycleRegistry.currentState = Lifecycle.State.CREATED
        }

        /** Получить ЖЦ */
        override fun getLifecycle(): Lifecycle {
            return lifecycleRegistry
        }
    }
}
