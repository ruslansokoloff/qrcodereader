package ru.morizo.qrcodereader.core.presentation_layer.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil

/**
 * Базовый класс для адаптера Recycler View с коллекцией элементов
 *
 * @property items Коллекция моделей для элементов RV
 * @property diffUtilCallback Callback для сравнения элементов списка при обновлении данных. Если
 * его передать, то обновление списка будет анимированным.
 */
abstract class BaseBindingAdapter<Model, VH : BaseBindingViewHolder<Model, out ViewDataBinding>> : LiveDataBindingRecyclerViewAdapter<VH>() {

    private var items: List<Model> = mutableListOf()

    var diffUtilCallback: BaseDiffUtilCallback<Model>? = null

    /**
     * Создание экземпляра ViewHolder-а
     */
    abstract fun createViewHolderInstance(parent: ViewGroup, viewType: Int): VH

    /**
     * Создание ViewHolder-а и добавление его как lifecycleOwner в binding
     * Основной элемент для работы binding с LiveData
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return createViewHolderInstance(parent, viewType).apply {
            binding.lifecycleOwner = this
        }
    }

    /**
     * Количество элементов
     */
    override fun getItemCount(): Int = items.size

    /**
     * Bind ViewHolder
     */
    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    /**
     * Получить элемент по индексу
     */
    fun getItem(position: Int): Model {
        return items[position]
    }

    /**
     * Создание класса отображаемого layout для элемента списка
     *
     * @param parent Контейнер
     * @param layoutResId Id layout ресурса
     *
     * @return Возвращает класс отображаемого layout для элемента списка
     */
    fun <Binding : ViewDataBinding> inflate(parent: ViewGroup, layoutResId: Int): Binding =
            DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    layoutResId,
                    parent,
                    false
            )

    /**
     * Метод для изменения списка данных
     */
    open fun updateItems(newItems: List<Model>) {
        val callback = diffUtilCallback
        if (callback != null) {
            DiffUtil.calculateDiff(callback.apply {
                setLists(items, newItems)
            }).also {
                updateList(newItems)
                it.dispatchUpdatesTo(this)
            }
        } else {
            updateList(newItems)
            notifyDataSetChanged()
        }
    }

    /**
     * Инициализировать список данных
     */
    private fun updateList(newItems: List<Model>) {
        items = newItems
    }
}
