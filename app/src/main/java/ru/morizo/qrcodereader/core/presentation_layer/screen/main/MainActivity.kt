package ru.morizo.qrcodereader.core.presentation_layer.screen.main

import android.content.Context
import android.content.Intent
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity.BaseActivity
import ru.morizo.qrcodereader.databinding.ActivityBaseBinding

class MainActivity : BaseActivity<MainActivityViewModel, ActivityBaseBinding>() {

    companion object {
        fun newIntent(context: Context?): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override val viewModelType = MainActivityViewModel::class.java

    override val layoutIdRes = R.layout.activity_base

    override val fragmentsContainerId = R.id.fragments_container

    override fun provideViewModel(): MainActivityViewModel {
        return MainActivityViewModel()
    }

    override fun inject() {}
}