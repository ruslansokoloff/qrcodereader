package ru.morizo.qrcodereader.core.presentation_layer.binding_adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.morizo.qrcodereader.core.presentation_layer.adapter.base.BaseBindingAdapter

/**
 * Биндинг для подстановки списка данных в RecyclerView
 */
@BindingAdapter("items")
fun <T> RecyclerView.bindItems(items: MutableList<T>?) {
    items?.let {
        @Suppress("UNCHECKED_CAST")
        try {
            if (adapter != null && adapter is BaseBindingAdapter<*, *>) {
                (adapter as BaseBindingAdapter<T, *>).updateItems(it)
            }
        } catch (t: Throwable) {
            throw IllegalStateException("Incoming adapter is an incompatible type")
        }
    }
}

/**
 * Биндинг для скролла RecyclerView к нужной позиции
 */
@BindingAdapter("currentVisiblePosition")
fun <T> RecyclerView.bindCurrentVisiblePosition(position: Int?) {
    smoothScrollToPosition(position ?: return)
}
