package ru.morizo.qrcodereader.core.data_layer.network.api

import retrofit2.http.Body
import retrofit2.http.POST
import ru.morizo.qrcodereader.core.data_layer.network.call_adpter.ApiData
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckInfoRequestBody
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckInfoResponseBody

interface MainApi {

    companion object {
        const val BASE_URL = "https://cheq-check.morizolabs.ru/api/v1/"
    }

    @POST("qrcode")
    fun loadCheckInfo(
        @Body request: CheckInfoRequestBody
    ): ApiData<CheckInfoResponseBody>
}