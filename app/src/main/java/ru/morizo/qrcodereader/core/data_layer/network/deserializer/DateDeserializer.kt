package ru.morizo.qrcodereader.core.data_layer.network.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.jvm.Throws

/**
 * Вспомогательный класс для сериализации полей типа Java.Date
 * Добавляется как адаптер к Gson в NetworkModule
 *
 * @property dateFormats Форматы даты, доступные для сериализации
 */
class DateDeserializer : JsonDeserializer<Date> {

    private val dateFormats = listOf(
        "yyyy-MM-dd'T'HH:mm:ss",
        "HH:mm:ss",
        "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
        "yyyy-MM-dd'T'HH:mm:ssZ",
        "yyyy-MM-dd'T'HH:mm:ss.SSS",
        "dd.MM.yyyy HH:mm",
        "dd MMMM yyyy",
        "dd.MM.yyyy",
        "yyyy-MM-dd"
    )

    /**
     * Десериализует дату, если она в поддерживаемом формате
     *
     * @return Возвращает объект Java.Date
     */
    @Throws(JsonParseException::class)
    override fun deserialize(
        element: JsonElement,
        arg1: Type,
        arg2: JsonDeserializationContext
    ): Date? {
        for (format in dateFormats) {
            try {
                return SimpleDateFormat(format, Locale.getDefault()).parse(element.asString.trim())
            } catch (e: ParseException) {
            }
        }
        try {
            return Date(element.asString.toLong())
        } catch (e: NumberFormatException) {
        }
        throw JsonParseException("Unable to parse date ${element.asString}")
    }
}
