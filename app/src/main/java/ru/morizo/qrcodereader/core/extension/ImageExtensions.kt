package ru.morizo.qrcodereader.core.extension

import android.graphics.*
import android.media.Image
import java.io.ByteArrayOutputStream

fun Image.cropToBitmap(previewSize: Int, maxSize: Int): Bitmap {
    val yBuffer = planes[0].buffer
    val vuBuffer = planes[2].buffer
    val ySize = yBuffer.remaining()
    val vuSize = vuBuffer.remaining()
    val nv21 = ByteArray(ySize + vuSize)
    yBuffer.get(nv21, 0, ySize)
    vuBuffer.get(nv21, ySize, vuSize)

    val yuvImage = YuvImage(nv21, ImageFormat.NV21, this.width, this.height, null)
    val out = ByteArrayOutputStream()

    yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), 100, out)
    val imageBytes = out.toByteArray()
    val bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

    val matrix = Matrix().apply {
        postRotate(90F)
    }

    val ratio =  minOf(bitmap.width, bitmap.height).toFloat() / maxSize
    val realPreviewSize = (previewSize * ratio).toInt()
    val horizontalOffset = (bitmap.width - realPreviewSize) / 2
    val verticalOffset = (bitmap.height - realPreviewSize) / 2

    return Bitmap.createBitmap(bitmap, horizontalOffset, verticalOffset, realPreviewSize, realPreviewSize, matrix, true)
}