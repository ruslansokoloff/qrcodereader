package ru.morizo.qrcodereader.core.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.morizo.qrcodereader.core.di.scope.PerApplication

@Module
class ApplicationModule(private val appContext: Context) {

    @Provides
    @PerApplication
    fun provideApplicationContext(): Context = appContext
}