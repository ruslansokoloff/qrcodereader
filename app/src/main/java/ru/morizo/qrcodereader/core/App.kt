package ru.morizo.qrcodereader.core

import android.app.Application
import androidx.multidex.MultiDex
import ru.morizo.qrcodereader.core.di.component.DaggerApplicationComponent
import ru.morizo.qrcodereader.core.di.module.ApiModule
import ru.morizo.qrcodereader.core.di.module.ApplicationModule
import ru.morizo.qrcodereader.core.di.module.DatabaseModule
import ru.morizo.qrcodereader.core.di.module.HttpModule
import ru.morizo.qrcodereader.scanner.di.component.DaggerScannerComponent
import ru.morizo.qrcodereader.scanner.di.component.ScannerComponent
import ru.morizo.qrcodereader.scanner.di.module.ScannerModule
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class App : Application() {

    companion object {
        lateinit var cicerone: Cicerone<Router>
            private set
    }

    lateinit var scannerComponent: ScannerComponent
        private set

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(applicationContext)
        initCicerone()
        initDagger()
    }

    /**
     * Инициализация роутинга
     */
    private fun initCicerone() {
        cicerone = Cicerone.create()
    }

    /**
     * Инициализация dagger-компонентов
     */
    private fun initDagger() {
        val applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(applicationContext))
            .databaseModule(DatabaseModule())
            .httpModule(HttpModule())
            .apiModule(ApiModule())
            .build()

        scannerComponent = DaggerScannerComponent.builder()
            .applicationComponent(applicationComponent)
            .scannerModule(ScannerModule())
            .build()
    }
}