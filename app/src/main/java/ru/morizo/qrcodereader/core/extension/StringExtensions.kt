package ru.morizo.qrcodereader.core.extension

fun String.asQueryMap(): Map<String?, String?> {
    return split("&").map {
        val map = it.split("=")
        Pair(map.firstOrNull(), map.lastOrNull())
    }.toMap()
}