package ru.morizo.qrcodereader.core.di.component

import android.content.Context
import com.google.gson.Gson
import dagger.Component
import ru.morizo.qrcodereader.core.data_layer.database.AppDatabase
import ru.morizo.qrcodereader.core.data_layer.network.api.MainApi
import ru.morizo.qrcodereader.core.di.module.ApiModule
import ru.morizo.qrcodereader.core.di.module.ApplicationModule
import ru.morizo.qrcodereader.core.di.module.DatabaseModule
import ru.morizo.qrcodereader.core.di.module.HttpModule
import ru.morizo.qrcodereader.core.di.scope.PerApplication

@PerApplication
@Component(
    modules = [
        ApplicationModule::class,
        DatabaseModule::class,
        HttpModule::class,
        ApiModule::class
    ]
)
interface ApplicationComponent {
    fun appContext(): Context
    fun gson(): Gson
    fun mainApi(): MainApi
    fun database(): AppDatabase
}