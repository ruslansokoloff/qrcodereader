package ru.morizo.qrcodereader.core.presentation_layer.screen.splash

import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity.ActivityViewModel

class SplashViewModel : ActivityViewModel() {

    fun onPermissionsGranted() {
        getRouter().newRootScreen(Screens.mainActivity())
    }

    fun onPermissionsDenied() {
        getRouter().exit()
    }
}