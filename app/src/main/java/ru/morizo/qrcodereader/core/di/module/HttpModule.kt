package ru.morizo.qrcodereader.core.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.morizo.qrcodereader.core.data_layer.network.call_adpter.ApiDataAdapter
import ru.morizo.qrcodereader.core.data_layer.network.deserializer.DateDeserializer
import ru.morizo.qrcodereader.core.di.scope.PerApplication
import java.util.*
import java.util.concurrent.TimeUnit

@Module
class HttpModule {

    @Provides
    @PerApplication
    fun provideGson(): Gson {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .serializeNulls()
            .registerTypeAdapter(Date::class.java, DateDeserializer())
            .enableComplexMapKeySerialization()
            .create()
    }

    @Provides
    @PerApplication
    fun provideHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .followRedirects(true)
            .followSslRedirects(true)
            .connectTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .readTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .writeTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .dispatcher(Dispatcher().apply {
                maxRequests = 1
            })

        return builder.build()
    }

    @Provides
    @PerApplication
    fun provideRetrofitBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(ApiDataAdapter.Factory())
    }

    companion object {
        const val CONNECTION_TIMEOUT = 30L
        val CONNECTION_TIMEOUT_UNIT = TimeUnit.SECONDS
    }
}
