package ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.inc_toolbar.view.*
import ru.morizo.qrcodereader.BR
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.BaseViewModelFactory
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity.BaseActivity
import ru.terrakok.cicerone.Navigator

abstract class BaseFragment<VM : FragmentViewModel, B : ViewDataBinding> : Fragment() {

    abstract val viewModelType: Class<VM>

    @get:LayoutRes
    abstract val layoutResId: Int

    val viewModel: VM by lazy {
        getViewModelInstance()
    }

    protected lateinit var binding: B

    private lateinit var navigator: Navigator

    abstract fun provideViewModel(): VM

    abstract fun inject()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        inject()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lifecycle.addObserver(viewModel)

        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    private fun getViewModelFactoryInstance(): BaseViewModelFactory<VM> {
        return object : BaseViewModelFactory<VM>() {
            override fun provideViewModel(): VM {
                return this@BaseFragment.provideViewModel().also {
                    it.onViewModelCreated(arguments)
                }
            }
        }
    }

    private fun getViewModelInstance(): VM {
        return ViewModelProvider(this, getViewModelFactoryInstance()).get(viewModelType)
    }

    protected fun getBaseActivity() = activity as BaseActivity<*, *>

    protected fun getApp() = getBaseActivity().getApp()

    protected fun setToolbarTitle(title: String?) {
        binding.root.toolbar?.title = title
    }

    protected fun setToolbarTitle(@StringRes resId: Int) {
        setToolbarTitle(getString(resId))
    }

    protected fun setToolbarMenu(@MenuRes resId: Int) {
        binding.root.toolbar?.apply {
            inflateMenu(resId)
            setOnMenuItemClickListener {
                onOptionsItemSelected(it)
            }
        }
    }

    protected fun setToolbarNavigationIcon(
        @DrawableRes resId: Int,
        callback: (() -> Boolean)? = null
    ) {
        binding.root.toolbar?.apply {
            setNavigationIcon(resId)
            setNavigationOnClickListener {
                val wasHandled = callback?.invoke() ?: false
                if (!wasHandled) {
                    activity?.onBackPressed()
                }
            }
        }
    }
}