package ru.morizo.qrcodereader.core.presentation_layer.screen.main

import android.os.Bundle
import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity.ActivityViewModel

class MainActivityViewModel : ActivityViewModel() {

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)
        getRouter().newRootScreen(Screens.scannerSelector())
    }
}