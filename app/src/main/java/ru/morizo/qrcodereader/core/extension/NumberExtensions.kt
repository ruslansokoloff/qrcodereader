package ru.morizo.qrcodereader.core.extension

import java.math.BigDecimal
import java.math.RoundingMode

fun Number.toRub(): Float {
    return BigDecimal((this.toDouble() / 100)).setScale(2, RoundingMode.HALF_UP).toFloat()
}