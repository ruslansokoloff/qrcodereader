package ru.morizo.qrcodereader.core.presentation_layer.adapter.base

import androidx.recyclerview.widget.DiffUtil


/**
 * Базовый callback для сравнивания списков для адаптера
 */
abstract class BaseDiffUtilCallback<Model> : DiffUtil.Callback() {

    private var oldList: List<Model> = mutableListOf()
    private var newList: List<Model> = mutableListOf()

    abstract fun areItemsTheSame(oldItem: Model, newItem: Model): Boolean
    abstract fun areContentsTheSame(oldItem: Model, newItem: Model): Boolean

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areItemsTheSame(oldList[oldItemPosition], newList[newItemPosition])
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areContentsTheSame(oldList[oldItemPosition], newList[newItemPosition])
    }

    fun setLists(oldList: List<Model>, newList: List<Model>) {
        this.oldList = oldList
        this.newList = newList
    }
}
