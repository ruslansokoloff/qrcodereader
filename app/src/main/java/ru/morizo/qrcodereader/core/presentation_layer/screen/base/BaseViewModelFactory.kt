package ru.morizo.qrcodereader.core.presentation_layer.screen.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseViewModelFactory<VM : BaseViewModel> : ViewModelProvider.Factory {

    abstract fun provideViewModel(): VM

    override fun <T : ViewModel> create(modelClass: Class<T>): T = provideViewModel() as T
}
