package ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment

import ru.morizo.qrcodereader.core.presentation_layer.screen.base.BaseViewModel

abstract class FragmentViewModel : BaseViewModel()