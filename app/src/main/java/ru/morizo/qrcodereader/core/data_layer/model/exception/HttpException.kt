package ru.morizo.qrcodereader.core.data_layer.model.exception

sealed class HttpException : Exception() {

    object EmptyBody: HttpException()

    object NetworkDisabled: HttpException()

    class Message(val displayMessage: String): HttpException()

    object Unknown: HttpException()
}