package ru.morizo.qrcodereader.core.data_layer.network.call_adpter

import okhttp3.ResponseBody
import retrofit2.Call
import ru.morizo.qrcodereader.core.data_layer.model.exception.HttpException
import java.net.UnknownHostException

class ApiData<T>(private val call: Call<T>) {

    @Throws(HttpException::class)
    fun execute(): T {
        val response = try {
            call.execute()
        } catch (e: Exception) {
            e.printStackTrace()
            throw handleCallException(e)
        }
        if (response.isSuccessful) {
            return response.body() ?: throw HttpException.EmptyBody
        }
        throw handleNetworkError(response.errorBody())
    }

    private fun handleNetworkError(body: ResponseBody?): HttpException {
        return HttpException.Unknown
    }

    private fun handleCallException(e: Exception): HttpException {
        return when (e) {
            is UnknownHostException -> HttpException.NetworkDisabled
            else -> HttpException.Unknown
        }
    }
}