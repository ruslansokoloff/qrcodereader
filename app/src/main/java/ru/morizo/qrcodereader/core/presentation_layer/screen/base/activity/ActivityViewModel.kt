package ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity

import ru.morizo.qrcodereader.core.presentation_layer.screen.base.BaseViewModel

abstract class ActivityViewModel : BaseViewModel()