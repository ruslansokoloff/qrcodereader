package ru.morizo.qrcodereader.core.presentation_layer.screen.base

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.morizo.qrcodereader.core.App
import ru.terrakok.cicerone.Router

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
    }

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    override fun onCleared() {
        super.onCleared()
        clearDisposables()
    }

    /**
     * Вызывается один раз при создании viewModel
     *
     * @param args агруемнты передаваемые во фрагмент или активити
     */
    open fun onViewModelCreated(args: Bundle?) {}

    /**
     * Возвращает Router навигации
     */
    protected fun getRouter(): Router {
        return App.cicerone.router
    }

    protected fun <T> toObservable(func: () -> T): Observable<T> {
        return Observable.fromCallable(func)
    }

    protected fun <T> subscribeObservable(
        func: () -> T,
        success: ((data: T) -> Unit)? = null,
        fail: ((throwable: Throwable) -> Unit)? = null,
        scheduler: Scheduler = Schedulers.io()
    ) {
        subscribeObservable(toObservable(func), success, fail, scheduler)
    }

    /**
     * Обернуть observable в нужный поток и подписаться
     */
    protected fun <T> subscribeObservable(
        observable: Observable<T>,
        success: ((data: T) -> Unit)? = null,
        fail: ((throwable: Throwable) -> Unit)? = null,
        scheduler: Scheduler = Schedulers.io()
    ) {
        val disposable = wrapObservable(observable, scheduler)
            .subscribe({
                success?.invoke(it)
            }, {
                it.printStackTrace()
                fail?.invoke(it)
            })
        addDisposable(disposable)
    }

    /**
     * Обернуть observable в нужный поток
     */
    protected fun <T> wrapObservable(
        observable: Observable<T>,
        scheduler: Scheduler = Schedulers.newThread()
    ): Observable<T> {
        return observable
            .subscribeOn(scheduler)
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Добавить disposable для его отмены после выхода с экрана
     */
    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    /**
     * Очистить compositeDisposable и отменить все потоки в нем
     */
    protected fun clearDisposables() {
        compositeDisposable.clear()
    }
}