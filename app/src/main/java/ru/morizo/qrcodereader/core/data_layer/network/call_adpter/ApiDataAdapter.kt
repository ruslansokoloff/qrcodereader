package ru.morizo.qrcodereader.core.data_layer.network.call_adpter

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class ApiDataAdapter<T>(
    private val responseType: Type
) : CallAdapter<T, Any> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<T>): Any = ApiData(call)

    class Factory : CallAdapter.Factory() {
        override fun get(
            returnType: Type?,
            annotations: Array<Annotation>,
            retrofit: Retrofit
        ): CallAdapter<*, *>? {
            return returnType?.let {
                return try {
                    // Get enclosing type
                    val enclosingType = (it as ParameterizedType)

                    // Ensure enclosing type is 'Simple'
                    if (enclosingType.rawType != ApiData::class.java)
                        null
                    else {
                        val type = enclosingType.actualTypeArguments[0]
                        ApiDataAdapter<Any>(type)
                    }
                } catch (ex: ClassCastException) {
                    null
                }
            }
        }
    }
}