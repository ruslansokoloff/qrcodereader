package ru.morizo.qrcodereader.core.presentation_layer.screen.splash

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.activity.BaseActivity
import ru.morizo.qrcodereader.databinding.ActivityBaseBinding

class SplashActivity : BaseActivity<SplashViewModel, ActivityBaseBinding>() {

    companion object {
        fun newIntent(context: Context?): Intent {
            return Intent(context, SplashActivity::class.java)
        }
    }

    override val viewModelType = SplashViewModel::class.java

    override val layoutIdRes = R.layout.activity_base

    override val fragmentsContainerId = R.id.fragments_container

    override fun provideViewModel(): SplashViewModel {
        return SplashViewModel()
    }

    override fun inject() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestPermissions()
    }

    private fun requestPermissions() {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    viewModel.onPermissionsGranted()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    viewModel.onPermissionsDenied()
                }
            })
            .check()
    }
}