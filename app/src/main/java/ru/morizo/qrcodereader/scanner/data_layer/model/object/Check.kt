package ru.morizo.qrcodereader.scanner.data_layer.model.`object`

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Check(
    var id: String?,
    var appliedTaxationType: Int?,
    var cashTotalSum: Int?,
    var code: Int?,
    var creditSum: Int?,
    var dateTime: Date?,
    var eCashTotalSum: Int?,
    var fiscalDocumentFormatVer: Int?,
    var fiscalDocumentNumber: Long?,
    var fiscalDriverNumber: String?,
    var fiscalSign: Long?,
    var fnsUrl: String?,
    var items: List<CheckItem>,
    var messageFiscalSign: String?,
    var nds18: Int?,
    var ndsNo: Int?,
    var ofdId: String?,
    var operationType: Int?,
    var operator: String,
    var prepaidSum: Int?,
    var provisionSum: Int?,
    var redefineMask: Int?,
    var requestNumber: Int?,
    var retailPlace: String,
    var retailPlaceAddress: String,
    var shiftNumber: Int?,
    var totalSum: Int?,
    var user: String?,
    var userInn: String?,
    var scanDate: Date?
): Parcelable