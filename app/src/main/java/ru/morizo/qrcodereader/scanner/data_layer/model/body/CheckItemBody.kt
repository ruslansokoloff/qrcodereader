package ru.morizo.qrcodereader.scanner.data_layer.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckItemBody {

    @Expose
    @SerializedName(value = "name")
    var name: String? = null

    @Expose
    @SerializedName(value = "nds")
    var nds: Int? = null

    @Expose
    @SerializedName(value = "payMethod")
    var payMethod: Int? = null

    @Expose
    @SerializedName(value = "paymentType")
    var paymentType: Int? = null

    @Expose
    @SerializedName(value = "price")
    var price: Int? = null

    @Expose
    @SerializedName(value = "quantity")
    var quantity: Float? = null

    @Expose
    @SerializedName(value = "sum")
    var sum: Int? = null
}