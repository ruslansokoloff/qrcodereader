package ru.morizo.qrcodereader.scanner.data_layer.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckInfoResponseBody {

    @Expose
    @SerializedName(value = "status")
    var status: Boolean? = null

    @Expose
    @SerializedName(value = "data")
    var data: String? = null
}