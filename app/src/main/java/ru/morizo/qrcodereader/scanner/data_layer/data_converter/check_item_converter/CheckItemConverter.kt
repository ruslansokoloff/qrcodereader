package ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_item_converter

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckItem
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckItemBody

interface CheckItemConverter {
    fun bodyToModel(body: CheckItemBody): CheckItem
    fun modelToBody(model: CheckItem): CheckItemBody
}