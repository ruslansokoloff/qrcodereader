package ru.morizo.qrcodereader.scanner.data_layer.database.type_converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckItemBody

class CheckItemBodyConverter {

    @TypeConverter
    fun checkItemsToString(list: List<CheckItemBody>?): String? {
        return list?.let { Gson().toJson(it) }
    }

    @TypeConverter
    fun stringToCheckItems(string: String?): List<CheckItemBody>? {
        return string?.let {
            val type = object : TypeToken<List<CheckItemBody>>() {}.type
            Gson().fromJson<List<CheckItemBody>>(it, type)
        } ?: emptyList()
    }
}