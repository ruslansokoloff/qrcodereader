package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_selector

import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.FragmentViewModel

class ScannerSelectorViewModel : FragmentViewModel() {

    fun onHistoryButtonClicked() {
        getRouter().navigateTo(Screens.checksHistory())
    }

    fun onMLKitButtonClicked() {
        getRouter().navigateTo(Screens.scanner())
    }

    fun onZxingButtonClicked() {
        getRouter().navigateTo(Screens.zxingScanner())
    }
}