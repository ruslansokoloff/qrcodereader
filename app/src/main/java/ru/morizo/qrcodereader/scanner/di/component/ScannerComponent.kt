package ru.morizo.qrcodereader.scanner.di.component

import dagger.Component
import ru.morizo.qrcodereader.core.di.component.ApplicationComponent
import ru.morizo.qrcodereader.core.di.scope.PerFeature
import ru.morizo.qrcodereader.scanner.di.module.ScannerModule
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.checks_history.ChecksHistoryFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner.ScannerFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_result.ScannerResultFragment
import ru.morizo.qrcodereader.scanner.presentation_layer.screen.zxing_scanner.ZxingScannerFragment

@Component(modules = [ScannerModule::class], dependencies = [ApplicationComponent::class])
@PerFeature
interface ScannerComponent {
    fun inject(fragment: ZxingScannerFragment)
    fun inject(fragment: ScannerResultFragment)
    fun inject(fragment: ChecksHistoryFragment)
}