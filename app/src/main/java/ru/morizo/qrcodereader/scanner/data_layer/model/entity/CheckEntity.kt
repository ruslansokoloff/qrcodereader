package ru.morizo.qrcodereader.scanner.data_layer.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckItemBody

@Entity(tableName = CheckEntity.TABLE_NAME)
class CheckEntity {

    @PrimaryKey
    @ColumnInfo(name = ID)
    var id: String = ""

    @ColumnInfo(name = "appliedTaxationType")
    var appliedTaxationType: Int? = null

    @ColumnInfo(name = "cashTotalSum")
    var cashTotalSum: Int? = null

    @ColumnInfo(name = "code")
    var code: Int? = null

    @ColumnInfo(name = "creditSum")
    var creditSum: Int? = null

    @ColumnInfo(name = "dateTime")
    var dateTime: Long? = null

    @ColumnInfo(name = "ecashTotalSum")
    var eCashTotalSum: Int? = null

    @ColumnInfo(name = "fiscalDocumentFormatVer")
    var fiscalDocumentFormatVer: Int? = null

    @ColumnInfo(name = "fiscalDocumentNumber")
    var fiscalDocumentNumber: Long? = null

    @ColumnInfo(name = "fiscalDriverNumber")
    var fiscalDriverNumber: String? = null

    @ColumnInfo(name = "fiscalSign")
    var fiscalSign: Long? = null

    @ColumnInfo(name = "fnsUrl")
    var fnsUrl: String? = null

    @ColumnInfo(name = "items")
    var items: List<CheckItemBody>? = null

    @ColumnInfo(name = "messageFiscalSign")
    var messageFiscalSign: String? = null

    @ColumnInfo(name = "nds18")
    var nds18: Int? = null

    @ColumnInfo(name = "ndsNo")
    var ndsNo: Int? = null

    @ColumnInfo(name = "ofdId")
    var ofdId: String? = null

    @ColumnInfo(name = "operationType")
    var operationType: Int? = null

    @ColumnInfo(name = "operator")
    var operator: String? = null

    @ColumnInfo(name = "prepaidSum")
    var prepaidSum: Int? = null

    @ColumnInfo(name = "provisionSum")
    var provisionSum: Int? = null

    @ColumnInfo(name = "redefineMask")
    var redefineMask: Int? = null

    @ColumnInfo(name = "requestNumber")
    var requestNumber: Int? = null

    @ColumnInfo(name = "retailPlace")
    var retailPlace: String? = null

    @ColumnInfo(name = "retailPlaceAddress")
    var retailPlaceAddress: String? = null

    @ColumnInfo(name = "shiftNumber")
    var shiftNumber: Int? = null

    @ColumnInfo(name = "totalSum")
    var totalSum: Int? = null

    @ColumnInfo(name = "user")
    var user: String? = null

    @ColumnInfo(name = "userInn")
    var userInn: String? = null

    @ColumnInfo(name = SCAN_DATE)
    var scanDate: Long? = null

    companion object {
        const val TABLE_NAME = "checks"
        const val ID = "id"
        const val SCAN_DATE = "scanDate"
    }
}