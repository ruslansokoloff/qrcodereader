package ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_history

import ru.morizo.qrcodereader.core.presentation_layer.adapter.base.BaseBindingViewHolder
import ru.morizo.qrcodereader.databinding.ItemCheckHistoryBinding
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckViewModel

class CheckHistoryViewHolder(
    binding: ItemCheckHistoryBinding
) : BaseBindingViewHolder<CheckViewModel, ItemCheckHistoryBinding>(binding)