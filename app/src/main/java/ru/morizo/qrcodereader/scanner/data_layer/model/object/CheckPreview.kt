package ru.morizo.qrcodereader.scanner.data_layer.model.`object`

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CheckPreview(
    val id: Long?,
    val timestamp: String?,
    val sum: Double?,
    val fn: String?,
    val fp: String?,
    val number: Int?
) : Parcelable {

    companion object {
        const val ID_KEY = "i"
        const val TIMESTAMP_KEY = "t"
        const val SUM_KEY = "s"
        const val FN_KEY = "fn"
        const val FP_KEY = "fp"
        const val NUMBER_KEY = "n"
    }

    fun toQueryString(): String {
        return "$ID_KEY=$id&$TIMESTAMP_KEY=$timestamp&$SUM_KEY=$sum&$FN_KEY=$fn&$FP_KEY=$fp&$NUMBER_KEY=$number"
    }
}