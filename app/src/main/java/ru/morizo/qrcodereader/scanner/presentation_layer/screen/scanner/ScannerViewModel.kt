package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner

import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.FragmentViewModel
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview

class ScannerViewModel: FragmentViewModel() {

    val checkAnalyzerListener: CheckAnalyzerListener = object : CheckAnalyzerListener {
        override fun onAnalyze(checkPreview: CheckPreview) {
            getRouter().navigateTo(Screens.scannerResult(checkPreview))
        }
    }
}