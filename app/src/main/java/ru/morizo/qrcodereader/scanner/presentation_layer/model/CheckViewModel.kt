package ru.morizo.qrcodereader.scanner.presentation_layer.model

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import java.text.SimpleDateFormat
import java.util.*

class CheckViewModel(
    val check: Check
) {

    fun displayDate() = check.dateTime?.let {
        SimpleDateFormat(
            "dd MMMM yyyy',' HH:mm:ss",
            Locale.getDefault()
        ).format(it)
    }

    fun displayScanDate() = check.scanDate?.let {
        SimpleDateFormat(
            "dd MMMM yyyy',' HH:mm",
            Locale.getDefault()
        ).format(it)
    }
}