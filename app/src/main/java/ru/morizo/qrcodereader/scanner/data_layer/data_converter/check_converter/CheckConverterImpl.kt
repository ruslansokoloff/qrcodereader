package ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_converter

import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_item_converter.CheckItemConverter
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckBody
import ru.morizo.qrcodereader.scanner.data_layer.model.entity.CheckEntity
import java.util.*

class CheckConverterImpl(
    private val checkItemConverter: CheckItemConverter
) : CheckConverter {

    override fun bodyToModel(body: CheckBody): Check = Check(
        id = body.kktRegId,
        appliedTaxationType = body.appliedTaxationType,
        cashTotalSum = body.cashTotalSum,
        code = body.code,
        creditSum = body.creditSum,
        dateTime = body.dateTime,
        eCashTotalSum = body.eCashTotalSum,
        fiscalDocumentFormatVer = body.fiscalDocumentFormatVer,
        fiscalDocumentNumber = body.fiscalDocumentNumber,
        fiscalDriverNumber = body.fiscalDriverNumber,
        fiscalSign = body.fiscalSign,
        fnsUrl = body.fnsUrl,
        items = body.items?.map { checkItemConverter.bodyToModel(it) } ?: emptyList(),
        messageFiscalSign = body.messageFiscalSign,
        nds18 = body.nds18,
        ndsNo = body.ndsNo,
        ofdId = body.ofdId,
        operationType = body.operationType,
        operator = body.operator ?: "",
        prepaidSum = body.prepaidSum,
        provisionSum = body.provisionSum,
        redefineMask = body.redefineMask,
        requestNumber = body.requestNumber,
        retailPlace = body.retailPlace ?: "",
        retailPlaceAddress = body.retailPlaceAddress ?: "",
        shiftNumber = body.shiftNumber,
        totalSum = body.totalSum,
        user = body.user,
        userInn = body.userInn,
        scanDate = Date(System.currentTimeMillis())
    )

    override fun entityToModel(entity: CheckEntity): Check = Check(
        id = entity.id,
        appliedTaxationType = entity.appliedTaxationType,
        cashTotalSum = entity.cashTotalSum,
        code = entity.code,
        creditSum = entity.creditSum,
        dateTime = entity.dateTime?.let { Date(it) },
        eCashTotalSum = entity.eCashTotalSum,
        fiscalDocumentFormatVer = entity.fiscalDocumentFormatVer,
        fiscalDocumentNumber = entity.fiscalDocumentNumber,
        fiscalDriverNumber = entity.fiscalDriverNumber,
        fiscalSign = entity.fiscalSign,
        fnsUrl = entity.fnsUrl,
        items = entity.items?.map { checkItemConverter.bodyToModel(it) } ?: emptyList(),
        messageFiscalSign = entity.messageFiscalSign,
        nds18 = entity.nds18,
        ndsNo = entity.ndsNo,
        ofdId = entity.ofdId,
        operationType = entity.operationType,
        operator = entity.operator ?: "",
        prepaidSum = entity.prepaidSum,
        provisionSum = entity.provisionSum,
        redefineMask = entity.redefineMask,
        requestNumber = entity.requestNumber,
        retailPlace = entity.retailPlace ?: "",
        retailPlaceAddress = entity.retailPlaceAddress ?: "",
        shiftNumber = entity.shiftNumber,
        totalSum = entity.totalSum,
        user = entity.user,
        userInn = entity.userInn,
        scanDate = entity.scanDate?.let { Date(it) }
    )

    override fun modelToEntity(model: Check): CheckEntity = CheckEntity().apply {
        id = model.id ?: ""
        appliedTaxationType = model.appliedTaxationType
        cashTotalSum = model.cashTotalSum
        code = model.code
        creditSum = model.creditSum
        dateTime = model.dateTime?.time
        eCashTotalSum = model.eCashTotalSum
        fiscalDocumentFormatVer = model.fiscalDocumentFormatVer
        fiscalDocumentNumber = model.fiscalDocumentNumber
        fiscalDriverNumber = model.fiscalDriverNumber
        fiscalSign = model.fiscalSign
        fnsUrl = model.fnsUrl
        items = model.items.map { checkItemConverter.modelToBody(it) }
        messageFiscalSign = model.messageFiscalSign
        nds18 = model.nds18
        ndsNo = model.ndsNo
        ofdId = model.ofdId
        operationType = model.operationType
        operator = model.operator
        prepaidSum = model.prepaidSum
        provisionSum = model.provisionSum
        redefineMask = model.redefineMask
        requestNumber = model.requestNumber
        retailPlace = model.retailPlace
        retailPlaceAddress = model.retailPlaceAddress
        shiftNumber = model.shiftNumber
        totalSum = model.totalSum
        user = model.user
        userInn = model.userInn
        scanDate = model.scanDate?.time
    }
}