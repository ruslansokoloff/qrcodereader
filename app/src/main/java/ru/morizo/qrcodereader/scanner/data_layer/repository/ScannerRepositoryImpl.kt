package ru.morizo.qrcodereader.scanner.data_layer.repository

import android.graphics.Bitmap
import com.google.gson.Gson
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.common.InputImage
import ru.morizo.qrcodereader.core.data_layer.model.exception.HttpException
import ru.morizo.qrcodereader.core.data_layer.network.api.MainApi
import ru.morizo.qrcodereader.core.extension.asQueryMap
import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_converter.CheckConverter
import ru.morizo.qrcodereader.scanner.data_layer.database.dao.CheckDao
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckBody
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckInfoRequestBody

class ScannerRepositoryImpl(
    private val gson: Gson,
    private val scanner: BarcodeScanner,
    private val mainApi: MainApi,
    private val checkDao: CheckDao,
    private val checkConverter: CheckConverter
) : ScannerRepository {

    override fun scanInputImage(inputImage: InputImage): List<CheckPreview> {
        val task = scanner.process(inputImage)
        while (!task.isComplete) { }
        val codes = if (task.isSuccessful) task.result else emptyList()
        return codes.mapNotNull { parseCheck(it.displayValue) }
    }

    override fun parseCheck(scanResult: String?): CheckPreview? {
        val data = scanResult?.asQueryMap() ?: emptyMap()
        if (
            !data.contains(CheckPreview.ID_KEY)
            || !data.contains(CheckPreview.TIMESTAMP_KEY)
            || !data.contains(CheckPreview.SUM_KEY)
            || !data.contains(CheckPreview.FN_KEY)
            || !data.contains(CheckPreview.FP_KEY)
            || !data.contains(CheckPreview.NUMBER_KEY)
        ) {
            return null
        }

        return CheckPreview(
            id = data[CheckPreview.ID_KEY]?.toLongOrNull(),
            timestamp = data[CheckPreview.TIMESTAMP_KEY],
            sum = data[CheckPreview.SUM_KEY]?.toDoubleOrNull(),
            fn = data[CheckPreview.FN_KEY],
            fp = data[CheckPreview.FP_KEY],
            number = data[CheckPreview.NUMBER_KEY]?.toIntOrNull()
        )
    }

    override fun loadCheckInfo(checkData: String?): Check {
        val request = CheckInfoRequestBody().apply {
            data = checkData
        }
        val response = mainApi.loadCheckInfo(request).execute()
        if (response.status != true)
            throw HttpException.EmptyBody

        val check = checkConverter.bodyToModel(gson.fromJson(response.data, CheckBody::class.java))
        checkDao.insert(checkConverter.modelToEntity(check))
        return check
    }

    override fun getChecksHistory(): List<Check> {
        return checkDao.getAll().map { checkConverter.entityToModel(it) }
    }

    override fun clearChecksHistory() {
        checkDao.clear()
    }
}