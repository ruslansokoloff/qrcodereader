package ru.morizo.qrcodereader.scanner.presentation_layer.screen.checks_history

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.FragmentViewModel
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckViewModel

class ChecksHistoryViewModel(
    private val scannerRepository: ScannerRepository
) : FragmentViewModel() {

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val items: MutableLiveData<List<CheckViewModel>> = MutableLiveData()

    val clickListener: (item: CheckViewModel) -> Unit = {
        getRouter().navigateTo(Screens.scannerResult(it.check))
    }

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)
        getHistory()
    }

    fun onClearButtonClicked() {
        clearHistory()
    }

    private fun getHistory() {
        isLoading.value = true
        subscribeObservable({ scannerRepository.getChecksHistory() }, { list ->
            items.value = list.map { CheckViewModel(it) }
            isLoading.value = false
        }, {
            items.value = emptyList()
            isLoading.value = false
        })
    }

    private fun clearHistory() {
        subscribeObservable({ scannerRepository.clearChecksHistory() }, {
            items.value = emptyList()
        }, {
            items.value = emptyList()
        })
    }
}