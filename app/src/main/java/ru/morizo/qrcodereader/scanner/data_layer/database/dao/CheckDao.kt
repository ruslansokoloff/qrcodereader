package ru.morizo.qrcodereader.scanner.data_layer.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.morizo.qrcodereader.scanner.data_layer.model.entity.CheckEntity

@Dao
interface CheckDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: CheckEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities: List<CheckEntity>)

    @Query("SELECT * FROM ${CheckEntity.TABLE_NAME} WHERE ${CheckEntity.ID} = :id")
    fun getOne(id: String?): CheckEntity?

    @Query("SELECT * FROM ${CheckEntity.TABLE_NAME} ORDER BY ${CheckEntity.SCAN_DATE} DESC")
    fun getAll(): List<CheckEntity>

    @Query("DELETE FROM ${CheckEntity.TABLE_NAME}")
    fun clear()
}