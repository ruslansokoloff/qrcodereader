package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner

import android.annotation.SuppressLint
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import ru.morizo.qrcodereader.core.extension.asQueryMap
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview

class CheckAnalyzer(
    private val listener: CheckAnalyzerListener
) : ImageAnalysis.Analyzer {

    private val scanner = BarcodeScanning.getClient()

    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        val image = imageProxy.image ?: return

        val inputImage = InputImage.fromMediaImage(image, imageProxy.imageInfo.rotationDegrees)
        scanner.process(inputImage)
            .addOnSuccessListener {
                val barcode = it.firstOrNull() ?: return@addOnSuccessListener
                val check = parseCheck(barcode.rawValue) ?: return@addOnSuccessListener
                listener.onAnalyze(check)
            }
            .addOnCompleteListener {
                imageProxy.close()
            }
    }

    private fun parseCheck(scanResult: String?): CheckPreview? {
        val data = scanResult?.asQueryMap() ?: emptyMap()
        if (
            !data.contains(CheckPreview.ID_KEY)
            || !data.contains(CheckPreview.TIMESTAMP_KEY)
            || !data.contains(CheckPreview.SUM_KEY)
            || !data.contains(CheckPreview.FN_KEY)
            || !data.contains(CheckPreview.FP_KEY)
            || !data.contains(CheckPreview.NUMBER_KEY)
        ) {
            return null
        }

        return CheckPreview(
            id = data[CheckPreview.ID_KEY]?.toLongOrNull(),
            timestamp = data[CheckPreview.TIMESTAMP_KEY],
            sum = data[CheckPreview.SUM_KEY]?.toDoubleOrNull(),
            fn = data[CheckPreview.FN_KEY],
            fp = data[CheckPreview.FP_KEY],
            number = data[CheckPreview.NUMBER_KEY]?.toIntOrNull()
        )
    }
}