package ru.morizo.qrcodereader.scanner.data_layer.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckInfoRequestBody {

    @Expose
    @SerializedName(value = "data")
    var data: String? = null
}