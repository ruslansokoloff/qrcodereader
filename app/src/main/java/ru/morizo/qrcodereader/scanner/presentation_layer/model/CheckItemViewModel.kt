package ru.morizo.qrcodereader.scanner.presentation_layer.model

import ru.morizo.qrcodereader.core.extension.toRub
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckItem

class CheckItemViewModel(
    val item: CheckItem
) {

    fun sumText(): String {
        return "${item.price?.toRub()} * ${item.quantity} = ${item.sum?.toRub()}"
    }

}