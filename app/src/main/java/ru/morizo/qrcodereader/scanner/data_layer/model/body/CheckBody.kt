package ru.morizo.qrcodereader.scanner.data_layer.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class CheckBody {

    @Expose
    @SerializedName(value = "appliedTaxationType")
    var appliedTaxationType: Int? = null

    @Expose
    @SerializedName(value = "cashTotalSum")
    var cashTotalSum: Int? = null

    @Expose
    @SerializedName(value = "code")
    var code: Int? = null

    @Expose
    @SerializedName(value = "creditSum")
    var creditSum: Int? = null

    @Expose
    @SerializedName(value = "dateTime")
    var dateTime: Date? = null

    @Expose
    @SerializedName(value = "ecashTotalSum")
    var eCashTotalSum: Int? = null

    @Expose
    @SerializedName(value = "fiscalDocumentFormatVer")
    var fiscalDocumentFormatVer: Int? = null

    @Expose
    @SerializedName(value = "fiscalDocumentNumber")
    var fiscalDocumentNumber: Long? = null

    @Expose
    @SerializedName(value = "fiscalDriverNumber")
    var fiscalDriverNumber: String? = null

    @Expose
    @SerializedName(value = "fiscalSign")
    var fiscalSign: Long? = null

    @Expose
    @SerializedName(value = "fnsUrl")
    var fnsUrl: String? = null

    @Expose
    @SerializedName(value = "items")
    var items: List<CheckItemBody>? = null

    @Expose
    @SerializedName(value = "kktRegId")
    var kktRegId: String? = null

    @Expose
    @SerializedName(value = "messageFiscalSign")
    var messageFiscalSign: String? = null

    @Expose
    @SerializedName(value = "nds18")
    var nds18: Int? = null

    @Expose
    @SerializedName(value = "ndsNo")
    var ndsNo: Int? = null

    @Expose
    @SerializedName(value = "ofdId")
    var ofdId: String? = null

    @Expose
    @SerializedName(value = "operationType")
    var operationType: Int? = null

    @Expose
    @SerializedName(value = "operator")
    var operator: String? = null

    @Expose
    @SerializedName(value = "prepaidSum")
    var prepaidSum: Int? = null

    @Expose
    @SerializedName(value = "provisionSum")
    var provisionSum: Int? = null

    @Expose
    @SerializedName(value = "redefine_mask")
    var redefineMask: Int? = null

    @Expose
    @SerializedName(value = "requestNumber")
    var requestNumber: Int? = null

    @Expose
    @SerializedName(value = "retailPlace")
    var retailPlace: String? = null

    @Expose
    @SerializedName(value = "retailPlaceAddress")
    var retailPlaceAddress: String? = null

    @Expose
    @SerializedName(value = "shiftNumber")
    var shiftNumber: Int? = null

    @Expose
    @SerializedName(value = "totalSum")
    var totalSum: Int? = null

    @Expose
    @SerializedName(value = "user")
    var user: String? = null

    @Expose
    @SerializedName(value = "userInn")
    var userInn: String? = null
}