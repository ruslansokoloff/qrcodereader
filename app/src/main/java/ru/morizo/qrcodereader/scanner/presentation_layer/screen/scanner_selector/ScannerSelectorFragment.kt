package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_selector

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.BaseFragment
import ru.morizo.qrcodereader.databinding.FragmentScannerSelectorBinding

class ScannerSelectorFragment :
    BaseFragment<ScannerSelectorViewModel, FragmentScannerSelectorBinding>() {

    companion object {
        fun newInstance(): ScannerSelectorFragment {
            return ScannerSelectorFragment()
        }
    }

    override val viewModelType = ScannerSelectorViewModel::class.java

    override val layoutResId = R.layout.fragment_scanner_selector

    override fun provideViewModel(): ScannerSelectorViewModel {
        return ScannerSelectorViewModel()
    }

    override fun inject() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_history -> viewModel.onHistoryButtonClicked()
        }
        return true
    }

    private fun initToolbar() {
        setToolbarTitle(R.string.select_scanner)
        setToolbarMenu(R.menu.history_menu)
    }
}