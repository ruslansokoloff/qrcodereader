package ru.morizo.qrcodereader.scanner.presentation_layer.custom_view.scanner

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageFormat
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.media.Image
import android.media.ImageReader
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.SurfaceHolder
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.extension.asQueryMap
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import ru.morizo.qrcodereader.scanner.presentation_layer.model.getPreviewOutputSize

class ScannerView : FrameLayout {

    companion object {
        private const val DEFAULT_IMAGE_BUFFER_SIZE = 1
        private const val BACK_CAMERA_FACING = 0
    }

    private var imageBufferSize = DEFAULT_IMAGE_BUFFER_SIZE
    private var cameraFacing = CameraCharacteristics.LENS_FACING_BACK
    private var borderSize: Int = -1
    private var maskColor: Int = -1

    private lateinit var viewFinder: AutoFitSurfaceView

    private val cameraManager: CameraManager by lazy {
        context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private val cameraId: String by lazy {
        cameraManager.cameraIdList.first {
            val characteristics = cameraManager.getCameraCharacteristics(it)
            characteristics.get(CameraCharacteristics.LENS_FACING) == cameraFacing
        }
    }

    private val characteristics: CameraCharacteristics by lazy {
        cameraManager.getCameraCharacteristics(cameraId)
    }

    private val imageReader: ImageReader by lazy {
        val size = characteristics
            .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            ?.getOutputSizes(ImageFormat.YUV_420_888)
            ?.maxBy { it.height * it.width }

        ImageReader.newInstance(
            size?.width ?: 0,
            size?.height ?: 0,
            ImageFormat.YUV_420_888,
            imageBufferSize
        )
    }

    private lateinit var scanner: BarcodeScanner
    private val disposables: CompositeDisposable = CompositeDisposable()
    private var scannerListener: ScannerListener? = null

    private var cameraDevice: CameraDevice? = null
    private var cameraSession: CameraCaptureSession? = null
    private var isLoading: Boolean = false

    private var currentWidth: Int = 0
    private var currentHeight: Int = 0

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(attrs)
    }

    constructor(context: Context) : super(context) {
        initView(null)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        currentWidth = w
        currentHeight = h
        //addMask()
        addBorder()
    }

    fun resumeScanning(listener: ScannerListener) {
        scannerListener = listener
        isLoading = false
    }

    fun stopScanning() {
        disposables.clear()
        closeCamera()
    }

    private fun initView(attrs: AttributeSet?) {
        viewFinder = AutoFitSurfaceView(context)
        addView(viewFinder, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))
        scanner = BarcodeScanning.getClient()

        getAttributes(attrs)

        imageReader.setOnImageAvailableListener({
            onGetImage(it)
        }, null)

        viewFinder.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceDestroyed(holder: SurfaceHolder) = Unit

            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) = Unit

            override fun surfaceCreated(holder: SurfaceHolder) {
                val previewSize = getPreviewOutputSize(
                    viewFinder.display,
                    characteristics,
                    SurfaceHolder::class.java
                )
                viewFinder.setAspectRatio(previewSize.width, previewSize.height)

                post { openCamera() }
            }
        })
    }

    private fun getAttributes(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ScannerView)
        imageBufferSize =
            attributes.getInt(R.styleable.ScannerView_imageBufferSize, DEFAULT_IMAGE_BUFFER_SIZE)
        cameraFacing =
            when (attributes.getInt(R.styleable.ScannerView_cameraFacing, BACK_CAMERA_FACING)) {
                0 -> CameraCharacteristics.LENS_FACING_BACK
                else -> CameraCharacteristics.LENS_FACING_FRONT
            }

        borderSize = attributes.getDimensionPixelOffset(R.styleable.ScannerView_borderSize, 0)
        maskColor = attributes.getColor(R.styleable.ScannerView_maskColor, -1)

        attributes.recycle()
    }

    private fun addBorder() {
        if (borderSize <= 0) return
        val borderView = View(context).apply {
            setBackgroundResource(R.drawable.ic_camera_lens)
        }
        val offset = resources.getDimensionPixelOffset(R.dimen.scanner_border_width) / 2
        addView(borderView, LayoutParams(borderSize + offset, borderSize + offset, Gravity.CENTER))
    }

    private fun addMask() {
        if (maskColor == -1) return
        if (borderSize <= 0) {
            View(context).apply { setBackgroundColor(maskColor) }
                .also {
                    addView(it, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))
                }
            return
        }

        val maskWidth = (currentWidth - borderSize) / 2
        val maskHeight = (currentHeight - borderSize) / 2

        View(context).apply { setBackgroundColor(maskColor) }.also {
            addView(it, LayoutParams(maskWidth, LayoutParams.MATCH_PARENT, Gravity.START))
        }
        View(context).apply { setBackgroundColor(maskColor) }.also {
            addView(it, LayoutParams(maskWidth, LayoutParams.MATCH_PARENT, Gravity.END))
        }
        View(context).apply { setBackgroundColor(maskColor) }.also {
            addView(it, LayoutParams(borderSize, maskHeight, Gravity.CENTER_HORIZONTAL or Gravity.TOP))
        }
        View(context).apply { setBackgroundColor(maskColor) }.also {
            addView(it, LayoutParams(borderSize, maskHeight, Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM))
        }
    }

    @SuppressLint("MissingPermission")
    private fun openCamera() {
        cameraManager.openCamera(cameraId, object : CameraDevice.StateCallback() {
            override fun onOpened(camera: CameraDevice) {
                cameraDevice = camera
                createCameraPreviewSession()
            }

            override fun onDisconnected(camera: CameraDevice) {
                closeCamera()
            }

            override fun onError(camera: CameraDevice, error: Int) {
                closeCamera()
            }
        }, null)
    }

    private fun closeCamera() {
        cameraDevice?.close()
        cameraDevice = null
        cameraSession?.close()
        cameraSession = null
    }

    private fun createCameraPreviewSession() {
        cameraDevice?.createCaptureSession(
            listOf(viewFinder.holder.surface, imageReader.surface),
            object : CameraCaptureSession.StateCallback() {
                override fun onConfigureFailed(session: CameraCaptureSession) {}
                override fun onConfigured(session: CameraCaptureSession) {
                    cameraSession = session
                    onCreateSession()
                }
            }, null
        )
    }

    private fun onCreateSession() {
        val builder = cameraDevice?.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)?.apply {
            addTarget(viewFinder.holder.surface)
            addTarget(imageReader.surface)
        } ?: return

        cameraSession?.setRepeatingRequest(builder.build(), null, null)
    }

    private fun onGetImage(reader: ImageReader) {
        val image: Image = reader.acquireLatestImage() ?: return
        if(isLoading) {
            image.close()
            return
        }
        isLoading = true
        val inputImage = InputImage.fromMediaImage(image, 0)
        image.close()

        val observable = Observable
            .fromCallable {
                scanInputImage(inputImage)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        val disposable = observable.subscribe({
            if (it.isNotEmpty()) {
                scannerListener?.onScanned(it.first())
            }
        }, {
            it.printStackTrace()
        })
        disposables.add(disposable)
    }

    private fun scanInputImage(inputImage: InputImage): List<CheckPreview> {
        val task = scanner.process(inputImage)
        while (!task.isComplete) { }
        val codes = if (task.isSuccessful) task.result else emptyList()
        return codes.mapNotNull { parseCheck(it.displayValue) }
    }

    private fun parseCheck(scanResult: String?): CheckPreview? {
        val data = scanResult?.asQueryMap() ?: emptyMap()
        if (
            !data.contains(CheckPreview.ID_KEY)
            || !data.contains(CheckPreview.TIMESTAMP_KEY)
            || !data.contains(CheckPreview.SUM_KEY)
            || !data.contains(CheckPreview.FN_KEY)
            || !data.contains(CheckPreview.FP_KEY)
            || !data.contains(CheckPreview.NUMBER_KEY)
        ) {
            return null
        }

        return CheckPreview(
            id = data[CheckPreview.ID_KEY]?.toLongOrNull(),
            timestamp = data[CheckPreview.TIMESTAMP_KEY],
            sum = data[CheckPreview.SUM_KEY]?.toDoubleOrNull(),
            fn = data[CheckPreview.FN_KEY],
            fp = data[CheckPreview.FP_KEY],
            number = data[CheckPreview.NUMBER_KEY]?.toIntOrNull()
        )
    }
}