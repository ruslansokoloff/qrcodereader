package ru.morizo.qrcodereader.scanner.di.module

import com.google.gson.Gson
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.zxing.qrcode.QRCodeReader
import dagger.Module
import dagger.Provides
import ru.morizo.qrcodereader.core.data_layer.database.AppDatabase
import ru.morizo.qrcodereader.core.data_layer.network.api.MainApi
import ru.morizo.qrcodereader.core.di.scope.PerFeature
import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_converter.CheckConverter
import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_converter.CheckConverterImpl
import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_item_converter.CheckItemConverter
import ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_item_converter.CheckItemConverterImpl
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepositoryImpl

@Module
class ScannerModule {

    @Provides
    @PerFeature
    fun provideCheckItemConverter(): CheckItemConverter {
        return CheckItemConverterImpl()
    }

    @Provides
    @PerFeature
    fun provideCheckConverter(checkItemConverter: CheckItemConverter): CheckConverter {
        return CheckConverterImpl(checkItemConverter)
    }

    @Provides
    @PerFeature
    fun provideBarcodeScanner(): BarcodeScanner {
        return BarcodeScanning.getClient()
    }

    @Provides
    @PerFeature
    fun provideQRCodeReader(): QRCodeReader {
        return QRCodeReader()
    }

    @Provides
    @PerFeature
    fun provideScannerRepository(
        gson: Gson,
        barcodeScanner: BarcodeScanner,
        mainApi: MainApi,
        database: AppDatabase,
        checkConverter: CheckConverter
    ): ScannerRepository {
        return ScannerRepositoryImpl(
            gson,
            barcodeScanner,
            mainApi,
            database.checkDao(),
            checkConverter
        )
    }
}