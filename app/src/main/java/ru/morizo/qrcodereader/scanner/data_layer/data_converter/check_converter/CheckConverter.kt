package ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_converter

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckBody
import ru.morizo.qrcodereader.scanner.data_layer.model.entity.CheckEntity

interface CheckConverter {
    fun bodyToModel(body: CheckBody): Check
    fun entityToModel(entity: CheckEntity): Check
    fun modelToEntity(model: Check): CheckEntity
}