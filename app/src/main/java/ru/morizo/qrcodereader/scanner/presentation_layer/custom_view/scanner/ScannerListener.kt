package ru.morizo.qrcodereader.scanner.presentation_layer.custom_view.scanner

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview

interface ScannerListener {
    fun onScanned(check: CheckPreview)
}