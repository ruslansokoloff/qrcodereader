package ru.morizo.qrcodereader.scanner.presentation_layer.screen.zxing_scanner

import androidx.lifecycle.MutableLiveData
import ru.morizo.qrcodereader.core.presentation_layer.model.Event
import ru.morizo.qrcodereader.core.presentation_layer.navigation.Screens
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.FragmentViewModel
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository

class ZxingScannerViewModel(
    private val scannerRepository: ScannerRepository
) : FragmentViewModel() {

    val resumeScannerEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()

    fun onGetScanResult(result: String?) {
        parseCheck(result)
    }

    private fun parseCheck(result: String?) {
        subscribeObservable({ scannerRepository.parseCheck(result) }, {
            getRouter().navigateTo(Screens.scannerResult(it ?: return@subscribeObservable))
        }, {
            resumeScannerEvent.value = Event(true)
        })
    }
}