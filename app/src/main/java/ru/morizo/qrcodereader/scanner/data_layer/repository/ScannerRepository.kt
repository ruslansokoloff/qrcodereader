package ru.morizo.qrcodereader.scanner.data_layer.repository

import com.google.mlkit.vision.common.InputImage
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview

interface ScannerRepository {

    fun scanInputImage(inputImage: InputImage): List<CheckPreview>

    fun parseCheck(scanResult: String?): CheckPreview?

    fun loadCheckInfo(checkData: String?): Check

    fun getChecksHistory(): List<Check>

    fun clearChecksHistory()
}