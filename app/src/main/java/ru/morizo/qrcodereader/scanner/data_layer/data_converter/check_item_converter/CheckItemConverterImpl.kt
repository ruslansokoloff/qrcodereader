package ru.morizo.qrcodereader.scanner.data_layer.data_converter.check_item_converter

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckItem
import ru.morizo.qrcodereader.scanner.data_layer.model.body.CheckItemBody

class CheckItemConverterImpl : CheckItemConverter {

    override fun bodyToModel(body: CheckItemBody): CheckItem = CheckItem(
        name = body.name,
        nds = body.nds,
        payMethod = body.payMethod,
        paymentType = body.paymentType,
        price = body.price,
        quantity = body.quantity,
        sum = body.sum
    )

    override fun modelToBody(model: CheckItem): CheckItemBody = CheckItemBody().apply {
        name = model.name
        nds = model.nds
        payMethod = model.payMethod
        paymentType = model.paymentType
        price = model.price
        quantity = model.quantity
        sum = model.sum
    }
}