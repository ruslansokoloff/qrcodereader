package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_result

import android.content.res.Resources
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.extension.toRub
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.FragmentViewModel
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckItemViewModel
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckViewModel

class ScannerResultViewModel(
    private val scannerRepository: ScannerRepository,
    private val resources: Resources
) : FragmentViewModel() {

    companion object {
        const val CHECK_PREVIEW = "check_preview"
        const val CHECK = "check"
    }

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isError: MutableLiveData<Boolean> = MutableLiveData()

    val check: MutableLiveData<CheckViewModel> = MutableLiveData()
    val checkItems: MutableLiveData<List<CheckItemViewModel>> = MutableLiveData()
    val totalSumText: MutableLiveData<String> = MutableLiveData()

    private var args: Bundle? = null

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)
        this.args = args
        init()
    }

    fun onRepeatButtonClicked() {
        init()
    }

    private fun init() {
        if (args?.containsKey(CHECK) == true) {
            val checkParam = args?.getParcelable<Check>(CHECK) as Check
            onGetCheck(checkParam)
        } else if (args?.containsKey(CHECK_PREVIEW) == true) {
            loadCheckData(args?.getParcelable<CheckPreview>(CHECK_PREVIEW) as CheckPreview)
        }
    }

    private fun loadCheckData(checkPreview: CheckPreview) {
        isLoading.value = true
        subscribeObservable({ scannerRepository.loadCheckInfo(checkPreview.toQueryString()) }, {
            isError.value = false
            isLoading.value = false
            onGetCheck(it)
        }, {
            isError.value = true
            isLoading.value = false
            check.value = null
        })
    }

    private fun onGetCheck(check: Check) {
        this.check.value = CheckViewModel(check)
        this.checkItems.value = check.items.map { CheckItemViewModel(it) }
        totalSumText.value = resources.getString(R.string.x_total_sum, check.totalSum?.toRub())
    }
}