package ru.morizo.qrcodereader.scanner.presentation_layer.screen.zxing_scanner

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.BaseFragment
import ru.morizo.qrcodereader.databinding.FragmentZxingScannerBinding
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import javax.inject.Inject

class ZxingScannerFragment : BaseFragment<ZxingScannerViewModel, FragmentZxingScannerBinding>(),
    ZXingScannerView.ResultHandler {

    companion object {
        fun newInstance(): ZxingScannerFragment {
            return ZxingScannerFragment()
        }
    }

    override val viewModelType = ZxingScannerViewModel::class.java

    override val layoutResId = R.layout.fragment_zxing_scanner

    @Inject
    lateinit var scannerRepository: ScannerRepository

    override fun provideViewModel(): ZxingScannerViewModel {
        return ZxingScannerViewModel(scannerRepository)
    }

    override fun inject() {
        getApp().scannerComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        observeData()
    }

    override fun handleResult(rawResult: Result?) {
        viewModel.onGetScanResult(rawResult?.text)
    }

    override fun onResume() {
        super.onResume()
        binding.scannerView.setResultHandler(this)
        binding.scannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        binding.scannerView.stopCamera()
    }

    private fun initToolbar() {
        setToolbarNavigationIcon(R.drawable.ic_back)
        setToolbarTitle(R.string.zxing_scanner)
    }

    private fun observeData() {
        viewModel.resumeScannerEvent.observe(viewLifecycleOwner, Observer {
            if (it.getContentIfNotHandled() == true) {
                binding.scannerView.resumeCameraPreview(this)
            }
        })
    }
}