package ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_items

import android.view.ViewGroup
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.adapter.base.BaseBindingAdapter
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckItemViewModel

class CheckItemsAdapter : BaseBindingAdapter<CheckItemViewModel, CheckItemViewHolder>() {

    override fun createViewHolderInstance(parent: ViewGroup, viewType: Int): CheckItemViewHolder {
        return CheckItemViewHolder(inflate(parent, R.layout.item_check_item))
    }
}