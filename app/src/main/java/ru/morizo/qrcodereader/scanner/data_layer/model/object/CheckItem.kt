package ru.morizo.qrcodereader.scanner.data_layer.model.`object`

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CheckItem(
    var name: String?,
    var nds: Int?,
    var payMethod: Int?,
    var paymentType: Int?,
    var price: Int?,
    var quantity: Float?,
    var sum: Int?
): Parcelable