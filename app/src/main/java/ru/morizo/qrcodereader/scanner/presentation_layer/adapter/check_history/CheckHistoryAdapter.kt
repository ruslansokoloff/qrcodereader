package ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_history

import android.view.ViewGroup
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.adapter.base.BaseBindingAdapter
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckViewModel

class CheckHistoryAdapter : BaseBindingAdapter<CheckViewModel, CheckHistoryViewHolder>() {

    var clickListener: ((item: CheckViewModel) -> Unit)? = null

    override fun createViewHolderInstance(
        parent: ViewGroup,
        viewType: Int
    ): CheckHistoryViewHolder {
        return CheckHistoryViewHolder(inflate(parent, R.layout.item_check_history))
    }

    override fun onBindViewHolder(holder: CheckHistoryViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        holder.binding.resultButton.setOnClickListener {
            clickListener?.invoke(getItem(position))
        }
    }
}