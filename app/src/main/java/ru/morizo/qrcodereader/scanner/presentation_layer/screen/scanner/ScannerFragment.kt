package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.BaseFragment
import ru.morizo.qrcodereader.databinding.FragmentScannerBinding
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean


class ScannerFragment : BaseFragment<ScannerViewModel, FragmentScannerBinding>() {

    companion object {
        fun newInstance(): ScannerFragment {
            return ScannerFragment()
        }
    }

    override val viewModelType = ScannerViewModel::class.java

    override val layoutResId = R.layout.fragment_scanner

    private lateinit var cameraExecutor: ExecutorService

    private val canBeHandled: AtomicBoolean = AtomicBoolean(true)

    override fun provideViewModel(): ScannerViewModel {
        return ScannerViewModel()
    }

    override fun inject() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()

        cameraExecutor = Executors.newSingleThreadExecutor()
        startCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    override fun onResume() {
        super.onResume()
        canBeHandled.set(true)
    }

    private fun initToolbar() {
        setToolbarTitle(R.string.mlkit_scanner)
        setToolbarNavigationIcon(R.drawable.ic_back)
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder().build()
            preview.setSurfaceProvider(binding.cameraView.surfaceProvider)

            val imageAnalysis = ImageAnalysis.Builder().build()
            imageAnalysis.setAnalyzer(
                cameraExecutor,
                CheckAnalyzer(object : CheckAnalyzerListener {
                    override fun onAnalyze(checkPreview: CheckPreview) {
                        if (canBeHandled.compareAndSet(true, false)) {
                            viewModel.checkAnalyzerListener.onAnalyze(checkPreview)
                        }
                    }
                })
            )

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    viewLifecycleOwner,
                    cameraSelector,
                    preview,
                    imageAnalysis
                )
            } catch (e: Exception) {
                Log.e("PreviewUseCase", "Binding failed! :(", e)
            }
        }, ContextCompat.getMainExecutor(requireContext()))
    }
}