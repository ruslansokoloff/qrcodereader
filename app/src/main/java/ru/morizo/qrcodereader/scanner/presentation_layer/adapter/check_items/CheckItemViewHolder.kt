package ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_items

import ru.morizo.qrcodereader.core.presentation_layer.adapter.base.BaseBindingViewHolder
import ru.morizo.qrcodereader.databinding.ItemCheckItemBinding
import ru.morizo.qrcodereader.scanner.presentation_layer.model.CheckItemViewModel

class CheckItemViewHolder(
    binding: ItemCheckItemBinding
) : BaseBindingViewHolder<CheckItemViewModel, ItemCheckItemBinding>(binding)