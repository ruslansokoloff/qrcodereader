package ru.morizo.qrcodereader.scanner.presentation_layer.screen.checks_history

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.BaseFragment
import ru.morizo.qrcodereader.databinding.FragmentChecksHistoryBinding
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_history.CheckHistoryAdapter
import javax.inject.Inject

class ChecksHistoryFragment : BaseFragment<ChecksHistoryViewModel, FragmentChecksHistoryBinding>() {

    companion object {
        fun newInstance(): ChecksHistoryFragment {
            return ChecksHistoryFragment()
        }
    }

    override val viewModelType = ChecksHistoryViewModel::class.java

    override val layoutResId = R.layout.fragment_checks_history

    @Inject
    lateinit var scannerRepository: ScannerRepository

    override fun provideViewModel(): ChecksHistoryViewModel {
        return ChecksHistoryViewModel(scannerRepository)
    }

    override fun inject() {
        getApp().scannerComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_clear -> viewModel.onClearButtonClicked()
        }
        return true
    }

    private fun initViews() {
        setToolbarNavigationIcon(R.drawable.ic_back)
        setToolbarTitle(R.string.history)
        setToolbarMenu(R.menu.clear_menu)

        val adapter = CheckHistoryAdapter()
        adapter.clickListener = viewModel.clickListener
        binding.historyRecyclerView.adapter = adapter
    }
}