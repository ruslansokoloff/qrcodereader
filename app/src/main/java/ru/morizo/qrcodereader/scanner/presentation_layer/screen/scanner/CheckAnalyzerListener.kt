package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner

import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview

interface CheckAnalyzerListener {
    fun onAnalyze(checkPreview: CheckPreview)
}