package ru.morizo.qrcodereader.scanner.presentation_layer.screen.scanner_result

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import ru.morizo.qrcodereader.R
import ru.morizo.qrcodereader.core.presentation_layer.screen.base.fragment.BaseFragment
import ru.morizo.qrcodereader.databinding.FragmentScannerResultBinding
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.Check
import ru.morizo.qrcodereader.scanner.data_layer.model.`object`.CheckPreview
import ru.morizo.qrcodereader.scanner.data_layer.repository.ScannerRepository
import ru.morizo.qrcodereader.scanner.presentation_layer.adapter.check_items.CheckItemsAdapter
import javax.inject.Inject

class ScannerResultFragment : BaseFragment<ScannerResultViewModel, FragmentScannerResultBinding>() {

    companion object {
        fun newInstance(checkPreview: CheckPreview): ScannerResultFragment {
            return ScannerResultFragment().apply {
                arguments = bundleOf(ScannerResultViewModel.CHECK_PREVIEW to checkPreview)
            }
        }

        fun newInstance(check: Check): ScannerResultFragment {
            return ScannerResultFragment().apply {
                arguments = bundleOf(ScannerResultViewModel.CHECK to check)
            }
        }
    }

    override val viewModelType = ScannerResultViewModel::class.java

    override val layoutResId = R.layout.fragment_scanner_result

    @Inject
    lateinit var scannerRepository: ScannerRepository

    override fun provideViewModel(): ScannerResultViewModel {
        return ScannerResultViewModel(scannerRepository, resources)
    }

    override fun inject() {
        getApp().scannerComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        setToolbarTitle(R.string.result)
        setToolbarNavigationIcon(R.drawable.ic_back)

        val adapter = CheckItemsAdapter()
        binding.checkItemsRecyclerView.adapter = adapter
    }
}